using System;
using UnityEngine;

namespace BlondeUtils
{
    [Serializable]
    public struct SerializableVector3
    {
        public float x, y, z;
        public SerializableVector3(float _x, float _y, float _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
        public static implicit operator Vector3(SerializableVector3 value)
        {
            return new Vector3(value.x, value.y, value.z);
        }
        public static implicit operator SerializableVector3(Vector3 value)
        {
            return new SerializableVector3(value.x, value.y, value.z);
        }
    }
}