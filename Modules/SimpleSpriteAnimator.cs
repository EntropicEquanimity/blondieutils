using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpriteRenderer))]
public class SimpleSpriteAnimator : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    [Range(1, 60)] public int framesPerSecond = 60;
    public bool DestroyOnEnd;
    public bool PlayOnAwake;
    public bool Loop;
    public bool HideOnEnd;
    public Sprite[] sprites;

    public UnityEvent OnStartPlay;
    public UnityEvent OnEndPlay;

    private int spriteIndex = 0;
    private void Awake()
    {
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        if (PlayOnAwake)
        {
            spriteRenderer.enabled = true;
            InvokeRepeating("SwitchSprites", 0, 1f / framesPerSecond);
        }
    }

    private void SwitchSprites()
    {
        if (spriteIndex == 0) { OnStartPlay.Invoke(); }
        if (sprites.Length == spriteIndex && DestroyOnEnd) { Destroy(gameObject); OnEndPlay.Invoke(); return; }
        if (sprites.Length == spriteIndex && HideOnEnd)
        {
            spriteRenderer.enabled = false;
            spriteIndex = 0;
            spriteRenderer.sprite = sprites[0];
            CancelInvoke();
            OnEndPlay.Invoke();
            return;
        }
        else if (Loop && sprites.Length == spriteIndex) { spriteIndex = 0; OnEndPlay.Invoke(); }
        else
        {
            spriteRenderer.sprite = sprites[spriteIndex];
            if (sprites.Length == spriteIndex)
            {
                CancelInvoke("SwitchSprites");
            }
            spriteIndex += 1;
        }
    }
    #region Public Methods
    public void Play()
    {
        spriteRenderer.enabled = true;
        InvokeRepeating("SwitchSprites", 0, 1f / framesPerSecond);
    }
    public void Pause()
    {
        CancelInvoke("SwitchSprites");
    }
    public void Restart()
    {
        spriteIndex = 0;
        spriteRenderer.sprite = sprites[spriteIndex];
    }
    public void GoToEnd()
    {
        CancelInvoke("SwitchSprites");
        spriteIndex = sprites.Length - 1;
        spriteRenderer.sprite = sprites[spriteIndex];
    }
    public void GoToNextImage()
    {
        if (sprites.Length == spriteIndex)
        {
            spriteIndex = 0;
            spriteRenderer.sprite = sprites[spriteIndex];
        }
        else
        {
            spriteIndex++;
            spriteRenderer.sprite = sprites[spriteIndex];
        }
    }
    public void SwitchToSpecificSprite(int _spriteIndex)
    {
        spriteIndex = _spriteIndex;
        spriteRenderer.sprite = sprites[spriteIndex];
    }
    #endregion
}
