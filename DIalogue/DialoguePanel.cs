﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialoguePanel : MonoBehaviour, IPointerClickHandler
{
    [Header("Typewriting Effect")]
    public float timeBetweenLetters = 0.1f;
    private bool isTypeWriting = false;

    [Header("Dialogue")]
    public Text dialogueText;
    public bool playOnStart = true;
    public string[] lines;
    public UnityEvent onDialogueStart;
    public UnityEvent onDialogueContinue;
    public UnityEvent onDialogueEnd;
    private int lineIndex;
    private bool endOfDialogue => lineIndex + 1 == lines.Length;

    private void Start()
    {
        if (playOnStart)
        {
            BeginDialogue();
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        InteractWithDialoguePanel();
    }
    public void BeginDialogue()
    {
        lineIndex = 0;
        if (lines.Length > 0)
        {
            StartCoroutine(nameof(TypewriterEffect));
        }
        onDialogueStart.Invoke();
    }
    public void EndDialogue()
    {
        onDialogueEnd.Invoke();
        gameObject.SetActive(false);
    }
    public void InteractWithDialoguePanel()
    {
        if (endOfDialogue && !isTypeWriting){ EndDialogue(); }
        else
        {
            if (isTypeWriting)
            {
                StopAllCoroutines();
                dialogueText.text = lines[lineIndex];
                isTypeWriting = false;
            }
            else
            {
                lineIndex++;
                StartCoroutine(nameof(TypewriterEffect));
                onDialogueContinue.Invoke();
            }
        }
    }
    public IEnumerator TypewriterEffect()
    {
        isTypeWriting = true;
        dialogueText.text = "";
        foreach(char c in lines[lineIndex])
        {
            dialogueText.text += c;
            yield return new WaitForSeconds(timeBetweenLetters);
        }
        isTypeWriting = false;
    }
}