# BlondieUtils

A collection of utils me and my friend(s) use for our projects. 

## HOW TO USE: 

1. Download the file and drag the file named 'Utils' into your Unity Project. I'd recommend that you put it in your scripts folder. (There is an Editor folder in there.)
2. Voila, you're good to go. 

## What Utils are in here exactly? 
### For Direct Coding
#### Attributes (Utils Folder)
- **DrawIf Attribute**: Only shows the field/variable if the conditions specified are true. <br>
``` [DrawIf("NAME_OF_BOOL_OR_ENUM_TO_COMPARE", VALUE_BEING_COMPARED)] ```
- **ShowOnly Attribute**: Basically ReadOnly. Makes a variable only visible in the inspector without any ability to edit. <br>
``` [ShowOnly] ```
- **Scene Attribute**: Enables user to drag a scene asset into a string field in the inspector. <br>
``` [Scene] public string YOUR_STRING_VARIABLE ```

#### Fields (Utils Folder)
- **SerializableVector3**: <br>
``` public SeriazableVector3 VECTOR3_VARIABLE; ```

### Other Utility Scripts 
**DialoguePanel.cs** (Dialogue Folder) <br>
Just drag this script onto a panel under a canvas and give it some variables to use.